<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    
    <title><?PHP echo $settings->bit_title.' | '; ?>Bits</title>
    
    <meta name="app-route" 	content="<?PHP echo get_controller().'/'.get_action(); ?>">
    <meta name="app-url" 	content="<?PHP echo home_url(); ?>">
    
    <?PHP echo asset( 'Bits.css' ); ?>
    
</head>
<body class="controller-<?PHP echo get_controller(); ?> action-<?PHP echo get_action(); ?>">
	<div class="data-wrap">
		<div id="top-bar">
			<nav>
				<ul>
					<li class="brand">
						<a href="<?PHP echo home_url(); ?>"><span class="bit-embed"></span> Selesti Bits</a>
					</li>
					
				</ul>
				
				<header>
					<div><?PHP echo _html($settings->bit_title); ?></p><?PHP if(isset($bit->version)){ echo '<span class="version"> &middot; Version '.$bit->version.'</span>'; }?> </div>
				</header>
			</nav>
		</div>
		<div id="share-wrap">
			<iframe src="<?PHP echo home_url('code/show/'.$bit->slug.'/'.$bit->version); ?>">
		</div>
	</div>
	<?PHP echo asset('jquery-1.9.0.min.js'); ?>
	<?PHP echo asset('Bits.js'); ?>
</body>